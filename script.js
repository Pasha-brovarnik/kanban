const addBtns = document.querySelectorAll('.add-btn:not(.solid)');
const saveItemBtns = document.querySelectorAll('.solid');
const addItemContainers = document.querySelectorAll('.add-container');
const addItems = document.querySelectorAll('.add-item');
// Item Lists
const listItem = document.querySelector('.drag-item');
const listColumns = document.querySelectorAll('.drag-item-list');
const backlogList = document.getElementById('backlog-list');
const progressList = document.getElementById('progress-list');
const completeList = document.getElementById('complete-list');
const onHoldList = document.getElementById('on-hold-list');

// Items
let updatedOnLoad = false;

// Initialize Arrays
let backlogListArray = [];
let progressListArray = [];
let completeListArray = [];
let onHoldListArray = [];
let listArrays = [];

// Drag Functionality
let draggedItem;
let dragging = false;
let currentColumn;

// Get Arrays from localStorage if available, set default values if not
function getSavedColumns() {
	if (localStorage.getItem('backlogItems')) {
		backlogListArray = JSON.parse(localStorage.backlogItems);
		progressListArray = JSON.parse(localStorage.progressItems);
		completeListArray = JSON.parse(localStorage.completeItems);
		onHoldListArray = JSON.parse(localStorage.onHoldItems);
	} else {
		backlogListArray = ['Release the course', 'Sit back and relax'];
		progressListArray = ['Work on projects', 'Listen to music'];
		completeListArray = ['Being cool', 'Getting stuff done'];
		onHoldListArray = ['Being uncool'];
	}
}

// Set localStorage Arrays
function updateSavedColumns() {
	listArrays = [
		backlogListArray,
		progressListArray,
		completeListArray,
		onHoldListArray,
	];

	const arrayNames = [
		'backlogItems',
		'progressItems',
		'completeItems',
		'onHoldItems',
	];

	arrayNames.forEach((arrayName, i) => {
		localStorage.setItem(arrayName, JSON.stringify(listArrays[i]));
	});
}

// Filter arrays to remove empty items
function filterArrays(array) {
	return array.filter((item) => item !== null);
}

function setCursorOnFireFox() {
	var range = document.createRange();
	var sel = window.getSelection();

	range.setStart(this.childNodes[0], this.childNodes[0].data.length);
	range.collapse(true);

	sel.removeAllRanges();
	sel.addRange(range);
}

// Create DOM Elements for each list item
function createItemEl(columnEl, column, item, index) {
	// List Item
	const listEl = document.createElement('li');
	listEl.classList.add('drag-item');
	listEl.draggable = true;
	listEl.setAttribute('ondragstart', 'drag(event)');

	const editableDiv = document.createElement('div');
	editableDiv.textContent = item;
	editableDiv.contentEditable = true;
	editableDiv.id = index;
	editableDiv.setAttribute('onfocusout', `updateItem(${index}, ${column})`);
	editableDiv.draggable;
	editableDiv.onclick = /Firefox/i.test(navigator.userAgent)
		? setCursorOnFireFox
		: null;

	const dragIcon = document.createElement('i');
	dragIcon.classList.add('fas', 'fa-bars');

	listEl.append(editableDiv, dragIcon);

	// Append
	columnEl.appendChild(listEl);
}

// Update Columns in DOM - Reset HTML, Filter Array, Update localStorage
function updateDOM() {
	// Check localStorage once
	if (!updatedOnLoad) {
		getSavedColumns();
	}
	// Backlog Column
	backlogList.textContent = '';
	backlogListArray.forEach((item, i) => {
		createItemEl(backlogList, 0, item, i);
	});
	backlogListArray = filterArrays(backlogListArray);
	// Progress Column
	progressList.textContent = '';
	progressListArray.forEach((item, i) => {
		createItemEl(progressList, 1, item, i);
	});
	progressListArray = filterArrays(progressListArray);
	// Complete Column
	completeList.textContent = '';
	completeListArray.forEach((item, i) => {
		createItemEl(completeList, 2, item, i);
	});
	completeListArray = filterArrays(completeListArray);
	// On Hold Column
	onHoldList.textContent = '';
	onHoldListArray.forEach((item, i) => {
		createItemEl(onHoldList, 3, item, i);
	});
	onHoldListArray = filterArrays(onHoldListArray);
	// Run getSavedColumns only once, Update Local Storage
	updatedOnLoad = true;
	updateSavedColumns();
}

// Update item - delete if necessary or update array value
function updateItem(id, column) {
	const selectedArray = listArrays[column];
	const selectColumnElm = listColumns[column].children;
	if (!dragging) {
		if (!selectColumnElm[id].textContent) {
			delete selectedArray[id];
		} else {
			selectedArray[id] = selectColumnElm[id].textContent;
		}
		updateDOM();
	}
}

// Add to column list, reset textbox
function addToColumn(column) {
	const itemText = addItems[column].textContent;
	const selectedArray = listArrays[column];
	selectedArray.push(itemText);
	addItems[column].textContent = '';
	updateDOM();
}

// Show add item input
function showInputBox(column) {
	addBtns[column].style.visibility = 'hidden';
	saveItemBtns[column].style.display = 'flex';
	addItemContainers[column].style.display = 'flex';
}

// Hide add item input
function hideInputBox(column) {
	addBtns[column].style.visibility = 'visible';
	saveItemBtns[column].style.display = 'none';
	addItemContainers[column].style.display = 'none';
	addToColumn(column);
}

// Allow arrays to reflect Drag and drop
function rebuildArrays() {
	backlogListArray = Array.from(backlogList.children).map(
		(item) => item.textContent
	);
	progressListArray = Array.from(progressList.children).map(
		(item) => item.textContent
	);
	completeListArray = Array.from(completeList.children).map(
		(item) => item.textContent
	);
	onHoldListArray = Array.from(onHoldList.children).map(
		(item) => item.textContent
	);

	updateDOM();
}

// Starts dragging
function drag(e) {
	draggedItem = e.target;
	draggedItem.contentEditable = false;
	dragging = true;
}

// When draged item enters drop area
function dragEnter(column) {
	listColumns[column].classList.add('over');
	currentColumn = column;
}

// Column allows for item to drop
function allowDrop(e) {
	e.preventDefault();
}

// Dropping item
function drop(e) {
	e.preventDefault();
	// Remove Background color and padding
	listColumns.forEach((column) => {
		column.classList.remove('over');
	});
	// Add item to column
	const parent = listColumns[currentColumn];
	parent.appendChild(draggedItem);
	draggedItem.contentEditable = true;
	dragging = false;
	rebuildArrays();
}

// Remove column highlight when dragged item leaves target area
document.addEventListener('dragleave', (e) => {
	if (e.target.className.includes('drag-item-list over')) {
		e.target.classList.remove('over');
	}
});

// On load
updateDOM();
